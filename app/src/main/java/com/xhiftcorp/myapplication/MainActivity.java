package com.xhiftcorp.myapplication;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private EditText titleEditText;
    private EditText textEditText;
    private EditText tagEditText;
    private EditText badgeEditText;
    private CheckBox autoCancelCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.send).setOnClickListener(this);
        findViewById(R.id.cancel).setOnClickListener(this);
        titleEditText = findViewById(R.id.title);
        textEditText = findViewById(R.id.text);
        tagEditText = findViewById(R.id.tag);
        badgeEditText = findViewById(R.id.badge);
        autoCancelCheckBox = findViewById(R.id.auto_cancel);

        createNotificationChannel();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                sendNotification();
                //setupNotificationChannel();
                break;
            case R.id.cancel:
                cancelTag();
                break;
        }
    }

    // This is the Notification Channel ID. More about this in the next section
    public static final String NOTIFICATION_CHANNEL_ID = "channel_id";

    //User visible Channel Name
    public static final String CHANNEL_NAME = "Notification Channel";

    private void createNotificationChannel() {
        // 알림 채널은 오레오 이상에서는 반드시 필수로 만들어야 하며, 알림은 만드시 하나의 채널에 속해야 한다.
        // 채널의 속성은 알림의 속성보다 우선하며, 채널의 속성은 안드로이드 설정에서 사용자가 바꿀 수 있다. 한번 만들어진 채널은 삭제만 가능하며 그 속성은 사용자만 바꿀 수 있고 앱이 속성을 바꿀 수 없다.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Importance applicable to all the notifications in this Channel
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            //Notification channel should only be created for devices running Android 26
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, CHANNEL_NAME, importance);
                //Boolean value to set if lights are enabled for Notifications from this Channel
                notificationChannel.enableLights(true);
                //Boolean value to set if vibration is enabled for Notifications from this Channel
                notificationChannel.enableVibration(true);
                //Sets the color of Notification Light
                notificationChannel.setLightColor(Color.GREEN);
                //Set the vibration pattern for notifications. Pattern is in milliseconds with the format {delay,play,sleep,play,sleep...}
                notificationChannel.setVibrationPattern(new long[]{
                        500,
                        500,
                        500,
                        500,
                        500
                });
                //Sets whether notifications from these Channel should be visible on Lockscreen or not
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                // Once the Notification Channel object is defined we can create the Channel as shown below
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    private void deleteNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID);
        }
    }

    public static final int NOTIFICATION_ID = 123;

    private void sendNotification() {
        Log.i(TAG, "sendNotification");

        String title = titleEditText.getText().toString();
        String text = textEditText.getText().toString();
        String tag = tagEditText.getText().toString();
        int badge = Integer.parseInt(badgeEditText.getText().toString());
        boolean autoCancel = autoCancelCheckBox.isChecked();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("tag", tag);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // 알림을 만들기 위해서는 NotificationCompat.Builder를 사용하여 알림의 여러 설정을 한 후 build()를 호출하면 만들어 진다
        NotificationCompat.Builder notificationCompatBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        // 알림을 클릭했을 때 자동으로 해당 알림을 지울지 여부를 선택한다. 자동 삭제가 아닌 경우 클릭만으로는 알람이 삭제되지 않고 수동으로 알림을 좌로 밀어서 삭제해야 한다.
        notificationCompatBuilder.setAutoCancel(autoCancel);

        // 알람에 보여줄 제목을 설정한다.
        notificationCompatBuilder.setContentTitle(title);

        // 알림에 보여줄 본문을 설정한다.
        notificationCompatBuilder.setContentText(text);

        // 화면 상단 status bar 영역에 표시할 알림 아이콘을 설정한다.
        notificationCompatBuilder.setSmallIcon(R.drawable.ic_stat_name);

        // 화면 상단 status bar를 쓸어 내리면 알림의 제목, 본문이 보이고 그 오른쪽에 보여질 썸네일 이미지를 설정한다.
        notificationCompatBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.room_1));

        // 알림의 펼쳤을 때 큰 이미지가 보이고, 위에서 표시했던 썸네일 이미지는 제거한다.
        notificationCompatBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.room_1)).bigLargeIcon(null));

        // 앱 아이콘 위에 표시할 배지 카운트를 설정한다. 1부터 설정 가능하며, 그 이하의 값이 설정되면 1이 표시된다.
        // 배지 카운트는 태그별로 합산하여 표시된다. 즉, tag1의 배지 카운트가 2이고, tag2의 배지 카운트가 3이면 실제 앱 아이콘 위에는 5가 표시된다.
        // 알림을 좌로 밀어 없애거나 자동 삭제 옵션을 준 후 알림을 클릭하면 해당 알람의 배지에 연계된 카운트 만큼 줄어든 숫자가 앱 아이콘 위에 표시된다.
        notificationCompatBuilder.setNumber(badge);

        // 알림을 클릭했을 때 전달할 인텐트를 설정한다.
        notificationCompatBuilder.setContentIntent(pendingIntent);

        // 만들어진 알림은 특정 태그를 붙여 시스템에 전달한다.
        // 태그 별로 알림이 독립적으로 표시되지만, 태그가 4개 이상 부터는 하나의 알림 아이콘으로 표시되고 대신 위에서 쓸어 내렸을 때 태그 별로 알림이 나열된다.
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(tag, NOTIFICATION_ID, notificationCompatBuilder.build());
    }

    private void setupNotificationChannel() {
        Intent intent = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
        //Unique Channel ID of the the particular Notification Channel
        intent.putExtra(Settings.EXTRA_CHANNEL_ID, NOTIFICATION_CHANNEL_ID);
        //Package Name
        intent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
        startActivity(intent);
    }

    private void cancelTag() {
        // 사용자가 알림을 삭제하지 않고 notification manager를 통해 특정 태그의 배지 카운트를 삭제하거나 전체 배지 카운트를 삭제할 수 있다.
        String tag = tagEditText.getText().toString();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(tag, NOTIFICATION_ID);
    }
}
